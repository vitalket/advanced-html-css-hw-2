import gulp from 'gulp';
import fs from 'fs';
import plumber from 'gulp-plumber';
import notify  from 'gulp-notify';
import changed from 'gulp-changed';
import rename from 'gulp-rename';
import clean from 'gulp-clean';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import autoprefixer from 'gulp-autoprefixer';
import cleanCSS from 'gulp-clean-css';
import groupCssMediaQueries from 'gulp-group-css-media-queries';

import concat from 'gulp-concat';
import minify from 'gulp-minify';

import imagemin from 'gulp-imagemin';

import browserSync from 'browser-sync';

const plumberNotify = (title) => {
    return {
        errorHandler: notify.onError({
            title: title,
            message: 'Error: <%= error.message %>',
            sound: false
        }),
    };
}

const cleanDist = (done) => {
    if (fs.existsSync('./dist/')) {
        return gulp.src('./dist/', {read: false})
            .pipe(clean({force: true}));
    }
    done();
}

const html = () => {
    return gulp.src('./src/*.html')
        .pipe(changed('./dist/'))
        .pipe(plumber(plumberNotify('HTML')))
        .pipe(gulp.dest('./dist/'));
}

const scss = () => {
    return gulp.src('./src/scss/*.scss')
        .pipe(changed('./dist/css/'))
        .pipe(plumber(plumberNotify('SCSS')))
        .pipe(autoprefixer())
        .pipe(groupCssMediaQueries())
        .pipe(sass())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/css/'))
}

const js = () => {
    return gulp.src('./src/js/**/*.js')
        .pipe(changed('./dist/js/'))
        .pipe(concat('scripts.js'))
        .pipe(minify({
            ext:{
            src:'.js',
            min:'.min.js'
        }
        }))
        .pipe(gulp.dest('./dist/js/'));
}

const images = () => {
    return gulp.src('./src/images/**/*')
        .pipe(changed('./dist/images'))
        .pipe(imagemin({verbose: true}))
        .pipe(gulp.dest('./dist/images'));
}

const files = () => {
    return gulp.src('./src/files/**/*')
        .pipe(changed('./dist/files/'))
        .pipe(gulp.dest('./dist/files/'))
}

const server = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
}

const watcher = () => {
    gulp.watch('./src/scss/**/*.scss', scss).on('change', browserSync.reload);
    gulp.watch('./src/**/*.html', html).on('change', browserSync.reload);
    gulp.watch('./src/images/**/*', images).on('change', browserSync.reload);
    gulp.watch('./src/js/**/*.js', js).on('change', browserSync.reload);
    gulp.watch('./src/files/**/*', files).on('change', browserSync.reload);
}

gulp.task('dev', gulp.series(
    gulp.parallel(html, scss, js, images, files),
    gulp.parallel(server, watcher)
));

gulp.task('build', gulp.series(
    cleanDist,
    gulp.parallel(html, scss, js, images, files)
));