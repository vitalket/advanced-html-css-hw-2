const burger = document.querySelector('.header__burger');
const menu = document.querySelector('.header__nav-list');

burger.addEventListener('click', () => {
    burger.classList.toggle('burger-active');
    menu.classList.toggle('menu-active');
    menu.classList.add('fadeInDown');
    document.body.classList.toggle('lock');
});